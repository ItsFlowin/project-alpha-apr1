from django.urls import path

from projects.views import (
    ProjectCreateView,
    ProjectDetailView,
    ProjectView,
)

urlpatterns = [
    path("", ProjectView.as_view(), name="list_projects"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
]
